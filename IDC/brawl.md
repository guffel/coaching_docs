# Brawl

The current meta in Brawl seems to be Ram, Tracer, Cass, Bap, and Lucio. On Brawl maps, this will be our default rollout. More on maps will be added at a later date.

![Alt text](./../pictures/brawl/ram_bap_cass.png)

This comp is really effective at applying main angle pressure via Ram, Bap, and Cass, while still being able to contest angles and set up pinches. We are lacking in range because of Cass's damage falloff. This comp is best played on maps where the sightlines are short to medium range with a lot of flank potential. It can generally have good matchups against any composition on the right map but can struggle against dive if you get hit during rotation.

So let's talk swaps. The hardest matchup is probably poke-dive (usually ball/doom comps), where we can just swap to a normal Winston dive and should win against that comp. Another option that can also work would be Dva/Orisa anti-dive. I will explain Orisa anti-dive later.

On maps/points where Cass can't get value because closing distance is too hard and the sightlines are just too long, swapping Cass to Soj is really reasonable. We can keep Bap as it is still a viable comp, but if we notice that we start losing Brawl fights, we can lean more into the poke playstyle and play Kiri.

![Alt text](./../pictures/brawl/ram_soj_kiri.png)

The definite advantage of this comp is the longer range and more flexibility in positioning, but we lose a lot of consistent damage, so we play a lot for picks from Soj/Tracer during turns.

Against Winston dive, I want us to swap to the following comp for now.

![Alt text](./../pictures/brawl/orisa_anti_dive.png)

[Orisa Anti Dive](./comps/orisa_anti_dive.md)

Against Brawl on more linear maps (where Tracer can't get easy backline access) and against Poke, we can swap to a full Brawl and try to win front to back with the following comp.

![Alt text](./../pictures/brawl/ram_full_brawl.png)

The swap from Cass to Soj still applies, but without the Kiri swap. If there are rotations that are too long, where we can just get poked out or get hit in the open by Tracer, we can swap Cass to Symm and skip the rotation time. We will discuss this for specific maps.

NOTE: This will be it for now. I will update this file if I think I want to change anything. If you are struggling against some specific archetype of comp, let me know, since I ignored Mauga/JQ and other comps for now. But this should be a general guideline on what swaps can be made according to general situations.



