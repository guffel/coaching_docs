# Poke

Poke compositions are particularly effective on long-range maps, where much of the map can be seen through LOS from advantageous positions. This makes it difficult for the enemy team to rotate and achieve their objectives. One classic variation of poke is as follows.

![Alt text](./../pictures/poke/sig_bap_zen.png)

All five heroes in a poke composition have the ability to deal significant damage. The specific benefit that Zenyatta brings is his burst damage potential through discord and his charged volley of orbs. This variation works best on linear poke maps like Circuit Royal. Meanwhile, on wider poke maps like Junkertown, Illari can be the preferred choice. Illari offers more self-sustainability to hold angles with her pylon, although this comes at the cost of some damage output.

![Alt text](./../pictures/poke/sig_bap_illari.png)
