# Dive

Let's start from the EU version of dive since noone can play Ana in EU it seems.

![Alt text](./../pictures/dive/winston_lucio_kiri.png)

This composition has the advantage of having no dedicated backline, allowing everyone to commit to a dive fully. This is particularly beneficial in a dive mirror scenario, as you can always fully engage without worrying about the consequences. Compared to the Ana and Brig backline, we lack some sustain for both the tank and DPS. Brig's Pack is particularly strong in DPS duels and more flexible than Kiriko's healing or Lucio's presence. Ana also provides the benefit of her nade, which can significantly enhance the lethality of a dive. The drawback is that it is much harder to maximize the value of Ana and Brigitte, and they are also harder to protect.

The decision to play Ana and Brigitte over Lucio and Kiriko usually depends on the map choice. Longer sightlines with accessible high ground favor Ana and Brig, while Lucio and Kiriko are better suited for more enclosed maps with shorter sightlines. A good example of this is Numbani: for the first point attack, Lucio and Kiriko are preferable, while for defense, Ana and Brigitte are more effective.

![Alt text](./../pictures/dive/winston_ana_brig.png)

Dive compositions are also quite flexible when it comes to DPS picks. In this case, we see Tracer and Echo due to their huge burst potential. They aim to capitalize on the spike of the fight to secure a quick pick. Other heroes that can fulfill this role include Genji and Sombra. These picks usually depend on the current meta, and while all of them are viable right now, Echo is probably considered the best pick because she can also burst tanks effectively.

Now let's talk about Sojourn in this dive composition. Sojourn offers a lot more poke while still maintaining burst damage potential, making her very effective against brawl-type compositions while also being viable in poke/dive matchups. Sojourn is an excellent pick on flat dive maps.

For now, I want us to focus on either playing Sojourn or Echo with dive, depending on the verticality of the map.