![Alt text](./../../pictures/brawl/orisa_anti_dive.png)

NOTE: there might be some swaps possible like Torb or even Reaper on some linear maps

The general focus of this composition is to consistently poke the enemy dive in the setup phase while minimizing the risk of death. Therefore, we play Orisa, Brigitte, and Baptiste to ensure survivability. Meanwhile, Orisa, Cassidy, and Baptiste offer significant poke against the enemy team with the potential to burst tanks. On longer-range maps, we can consider swapping Cassidy for Ashe, as she provides consistent pressure at a greater distance, albeit with a slight damage trade-off in closer ranges. Ultimately, either option works, so this should be a map-specific choice. Tracer is picked to pressure and ensure we don't lose all angles against the dive or at least lose them slowly, allowing us to poke someone else out in the meantime. Additionally, Tracer can win sidelane duels with the help of health packs.

Orisa should anchor in front of the core (Baptiste, Cassidy, Brigitte) where we scout the enemy team is staging their dive, making it more difficult for them to execute. If the enemy team takes the Orisa bait, we should be able to keep Orisa alive through the dive and let Baptiste and Cassidy eliminate everything in sight. Orisa will still need cover and sightlines to the backline to survive and be able to peel with her spear if needed.

The three-man core of Baptiste, Brigitte, and Cassidy should ensure that Cassidy can shoot consistently and poke the enemy team whenever and wherever they appear. Brigitte also needs to keep tabs on Tracer, so a hybrid positioning between Tracer and the core is acceptable.

Tracer's primary role is to mirror the enemy DPS to deny angles. When the fight escalates and the enemy team dives, Tracer should engage the enemy backline or peel for the core.

In general, we should avoid using any sustain abilities in neutral, as our poke is significantly stronger. Using Immortality Field, Fortify, and potentially even Regen Burst, Roll and Javelin Spin is troll. Everything else is fair game to exert pressure on the map.

The carry of the composition is very much Cassidy/Ashe and works best on more flat maps for vertical maps a Dva variation of this antidive would be more optimal.